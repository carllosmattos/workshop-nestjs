import { ConfigService } from 'src/config/config.service';
import { DataSource } from 'typeorm';

export const databaseProviders = [
  {
    provide: 'DATA_SOURCE',
    useFactory: async (configService: ConfigService) => {
      const dataSource = new DataSource({
        type: configService.envConfig.dbConnection,
        host: configService.envConfig.dbHost,
        port: configService.envConfig.dbPort,
        username: configService.envConfig.dbUsername,
        password: configService.envConfig.dbPassword,
        database: configService.envConfig.dbDatabase,
        entities: [__dirname + '/../**/*.entity.js'],
        synchronize: true,
      });

      return dataSource.initialize();
    },
    inject: [ConfigService],
  },
];
