import { IsEmail, MaxLength, MinLength } from 'class-validator';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 100 })
  name: string;

  @Column()
  documentType: Document;

  @Column({ unique: true })
  @MinLength(11)
  @MaxLength(14)
  documentNumber: string;

  @Column({ unique: true })
  @IsEmail()
  email: string;

  @Column()
  password: string;
}

export enum Document {
  CPF = 'CPF',
  CNPJ = 'CNPJ',
}
